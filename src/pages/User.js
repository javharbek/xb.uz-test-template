import React from 'react';
import '../assets/site.min.css';

import Card from '@material-ui/core/Card';
import AddIcon from '@material-ui/icons/Add';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
function User(){
    return(
            <div className="flex" style={{}}>
                <div className="d-flex flex fixed-content" style={{}}>
                    <div className="fade aside aside-sm" id="content-aside">
                        <div className="modal-dialog d-flex flex-column w-md bg-body" id="user-nav">
                            <div className="navbar">
                                <span className="text-md mx-2">Groups</span>
                                <div className="dropdown dropright">
                                    <button  className="btn btn-sm btn-icon no-bg no-shadow">
                                        <AddIcon/>
                                    </button>
                                    <div className="dropdown-menu w-lg">
                                        <div className="p-3">
                                            <div className="input-group">
                                                <input
                                                    type="text"
                                                    className="form-control form-control-sm"
                                                    id="newField"
                                                    placeholder="Group name"
                                                    required
                                                />
                                                <span className="input-group-append">
                                                    <button className="btn btn-white no-shadow btn-sm" type="button" id="newBtn">
                                                        <i data-feather="check"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="scrollable hover">
                                <div className="sidenav p-2">
                                    <nav className="nav-active-text-primary" data-nav>
                                        <ul className="nav">
                                            <li>
                                                <a href="#">
                                                    <span className="nav-text">All</span>
                                                    <span className="nav-badge">
                                                        <b className="badge badge-sm badge-pill gd-danger">15</b>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span className="nav-text">Company</span>
                                                    <span className="nav-badge">
                                                        <b className="badge badge-sm badge-pill gd-info">3</b>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span className="nav-text">Personal</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span className="nav-text">Team</span>
                                                </a>
                                            </li>
                                            <li className="nav-header hidden-folded mt-2">
                                                <span className="d-block pt-1 text-sm text-muted">Tags</span>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span className="mx-2">
                                                        <b className="badge badge-circle sm text-primary"></b>
                                                    </span>
                                                    <span className="nav-text">Clients</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span className="mx-2">
                                                        <b className="badge badge-circle sm text-info"></b>
                                                    </span>
                                                    <span className="nav-text">Suppliers</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span className="mx-2">
                                                        <b className="badge badge-circle sm text-success"></b>
                                                    </span>
                                                    <span className="nav-text">Competitors</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span className="mx-2">
                                                        <b className="badge badge-circle sm text-warning"></b>
                                                    </span>
                                                    <span className="nav-text">Corps</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="d-flex flex" id="content-body">
                        <div className="d-flex flex-column flex" id="user-list" data-plugin="user">
                            <div className="p-3">
                                <div className="toolbar">
                                    <div className="btn-group">
                                        <button
                                            className="btn btn-sm btn-icon btn-white"
                                            data-toggle="tooltip"
                                            title="Trash"
                                            id="btn-trash"
                                        >
                                            <DeleteOutlineIcon/>

                                        </button>
                                        <button
                                            className="btn btn-sm btn-icon btn-white sort"
                                            data-sort="item-author"
                                            data-toggle="tooltip"
                                            title="Sort"
                                        >
                                            <i className="sorting"></i>
                                        </button>
                                    </div>
                                    <div className="dropdown">
                                        <button className="btn btn-sm btn-white no-wrap" data-toggle="dropdown">A-Z</button>
                                        <div className="dropdown-menu p-3" id="filter">
                                            <span className="badge pointer">A</span>
                                            <span className="badge pointer">B</span>
                                            <span className="badge pointer">C</span>
                                            <span className="badge pointer">D</span>
                                            <span className="badge pointer">E</span>
                                            <span className="badge pointer">F</span>
                                            <span className="badge pointer">G</span>
                                            <span className="badge pointer">H</span>
                                            <span className="badge pointer">I</span>
                                            <span className="badge pointer">J</span>
                                            <span className="badge pointer">K</span>
                                            <span className="badge pointer">L</span>
                                            <span className="badge pointer">M</span>
                                            <span className="badge pointer">N</span>
                                            <span className="badge pointer">O</span>
                                            <span className="badge pointer">P</span>
                                            <span className="badge pointer">Q</span>
                                            <span className="badge pointer">R</span>
                                            <span className="badge pointer">S</span>
                                            <span className="badge pointer">T</span>
                                            <span className="badge pointer">U</span>
                                            <span className="badge pointer">V</span>
                                            <span className="badge pointer">W</span>
                                            <span className="badge pointer">X</span>
                                            <span className="badge pointer">Y</span>
                                            <span className="badge pointer">Z</span>
                                        </div>
                                    </div>
                                    <form className="flex">
                                        <div className="input-group">
                                            <input
                                                type="text"
                                                className="form-control form-control-theme form-control-sm search"
                                                placeholder="Search"
                                                required
                                            />
                                            <span className="input-group-append">
                                                <button className="btn btn-white no-border btn-sm" type="button">
                                                    <span className="d-flex text-muted">
                                                        <i data-feather="search"></i>
                                                    </span>
                                                </button>
                                            </span>
                                        </div>
                                    </form>
                                    <button
                                        data-toggle="modal"
                                        data-target="#content-aside"
                                        data-modal
                                        className="btn btn-sm btn-icon btn-white d-md-none"
                                    >
                                        <i data-feather="menu"></i>
                                    </button>
                                </div>
                            </div>
                            <Card className="scroll-y mx-3">
                                <div className="list list-row">
                                    <div className="list-item" data-id="7">
                                        <div>
                                            <label className="ui-check m-0">
                                                <input type="checkbox" name="id" value="7"/>
                                                <i></i>
                                            </label>
                                        </div>
                                        <div>
                                            <a href="#" data-toggle-class>
                                                <BookmarkBorderIcon/>
                                                </a>
                                        </div>
                                        <div>
                                            <a href="#">
                                                <span className="w-40 avatar gd-primary" data-toggle-className="loading">
                                                    <span className="avatar-status on b-white avatar-right"></span>
                                                    <img src="../assets/img/a7.jpg" alt="."/>
                                                </span>
                                            </a>
                                        </div>
                                        <div className="flex">
                                            <a href="#" className="item-author text-color">Frank Kelley</a>
                                            <div className="item-mail text-muted h-1x d-none d-sm-block">frank-kelley@lostcorp.com</div>
                                            <div className="item-tag tag hide">Suppliers,Work,Clients,Company,Team,Personal,Partners,Friends</div>
                                        </div>
                                        <div>
                                            <div className="item-action dropdown">
                                                <a href="#" data-toggle="dropdown" className="text-muted">
                                                    <MoreVertIcon/>
                                                </a>
                                                <div className="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                                    <a className="dropdown-item" href="#">See detail</a>
                                                    <a className="dropdown-item download">Download</a>
                                                    <a className="dropdown-item edit">Edit</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item trash">Delete item</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="list-item" data-id="1">
                                        <div>
                                            <label className="ui-check m-0">
                                                <input type="checkbox" name="id" value="1"/>
                                                <i></i>
                                            </label>
                                        </div>
                                        <div>
                                            <a href="#" data-toggle-class>
                                                <BookmarkBorderIcon/>
                                                </a>
                                        </div>
                                        <div>
                                            <a href="#">
                                                <span className="w-40 avatar gd-primary" data-toggle-className="loading">
                                                    <span className="avatar-status on b-white avatar-right"></span>
                                                    <img src="../assets/img/a1.jpg" alt="."/>
                                                </span>
                                            </a>
                                        </div>
                                        <div className="flex">
                                            <a href="#" className="item-author text-color">Joyce McCoy</a>
                                            <div className="item-mail text-muted h-1x d-none d-sm-block">joyce-mccoy@samsuminc..com</div>
                                            <div className="item-tag tag hide">Team,Suppliers,Work,Partners,Personal,Friends,Company,Clients</div>
                                        </div>
                                        <div>
                                            <div className="item-action dropdown">
                                                <a href="#" data-toggle="dropdown" className="text-muted">
                                                    <MoreVertIcon/>
                                                </a>
                                                <div className="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                                    <a className="dropdown-item" href="#">See detail</a>
                                                    <a className="dropdown-item download">Download</a>
                                                    <a className="dropdown-item edit">Edit</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item trash">Delete item</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="list-item" data-id="2">
                                        <div>
                                            <label className="ui-check m-0">
                                                <input type="checkbox" name="id" value="2"/>
                                                <i></i>
                                            </label>
                                        </div>
                                        <div>
                                            <a href="#" data-toggle-class>
                                                <BookmarkBorderIcon/>
                                                </a>
                                        </div>
                                        <div>
                                            <a href="#">
                                                <span className="w-40 avatar gd-primary" data-toggle-className="loading">
                                                    <span className="avatar-status off b-white avatar-right"></span>
                                                    <img src="../assets/img/a2.jpg" alt="."/>
                                                </span>
                                            </a>
                                        </div>
                                        <div className="flex">
                                            <a href="#" className="item-author text-color">Kathy Alexander</a>
                                            <div className="item-mail text-muted h-1x d-none d-sm-block">kathy-alexander@microsoft.com</div>
                                            <div className="item-tag tag hide">Friends,Suppliers,Team,Company,Clients,Personal,Work,Partners</div>
                                        </div>
                                        <div>
                                            <div className="item-action dropdown">
                                                <a href="#" data-toggle="dropdown" className="text-muted">
                                                    <MoreVertIcon/>
                                                </a>
                                                <div className="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                                    <a className="dropdown-item" href="#">See detail</a>
                                                    <a className="dropdown-item download">Download</a>
                                                    <a className="dropdown-item edit">Edit</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item trash">Delete item</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="list-item" data-id="17">
                                        <div>
                                            <label className="ui-check m-0">
                                                <input type="checkbox" name="id" value="17"/>
                                                <i></i>
                                            </label>
                                        </div>
                                        <div>
                                            <a href="#" data-toggle-class>
                                                <BookmarkBorderIcon/>
                                                </a>
                                        </div>
                                        <div>
                                            <a href="#">
                                                <span className="w-40 avatar gd-warning" data-toggle-className="loading">
                                                    <span className="avatar-status on b-white avatar-right"></span> A</span>
                                            </a>
                                        </div>
                                        <div className="flex">
                                            <a href="#" className="item-author text-color">Alan Mendez</a>
                                            <div className="item-mail text-muted h-1x d-none d-sm-block">alan-mendez@alibaba.com</div>
                                            <div className="item-tag tag hide">Suppliers,Team,Personal,Friends,Company,Work,Partners,Clients</div>
                                        </div>
                                        <div>
                                            <div className="item-action dropdown">
                                                <a href="#" data-toggle="dropdown" className="text-muted">
                                                    <MoreVertIcon/>
                                                </a>
                                                <div className="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                                    <a className="dropdown-item" href="#">See detail</a>
                                                    <a className="dropdown-item download">Download</a>
                                                    <a className="dropdown-item edit">Edit</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item trash">Delete item</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="list-item" data-id="16">
                                        <div>
                                            <label className="ui-check m-0">
                                                <input type="checkbox" name="id" value="16"/>
                                                <i></i>
                                            </label>
                                        </div>
                                        <div>
                                            <a href="#" data-toggle-class>
                                                <BookmarkBorderIcon/>
                                                </a>
                                        </div>
                                        <div>
                                            <a href="#">
                                                <span className="w-40 avatar gd-info" data-toggle-className="loading">
                                                    <span className="avatar-status on b-white avatar-right"></span> F</span>
                                            </a>
                                        </div>
                                        <div className="flex">
                                            <a href="#" className="item-author text-color">Frances Stewart</a>
                                            <div className="item-mail text-muted h-1x d-none d-sm-block">frances-stewart@microsoft.com</div>
                                            <div className="item-tag tag hide">Friends,Suppliers,Partners,Team,Work,Clients,Company,Personal</div>
                                        </div>
                                        <div>
                                            <div className="item-action dropdown">
                                                <a href="#" data-toggle="dropdown" className="text-muted">
                                                    <MoreVertIcon/>
                                                </a>
                                                <div className="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                                    <a className="dropdown-item" href="#">See detail</a>
                                                    <a className="dropdown-item download">Download</a>
                                                    <a className="dropdown-item edit">Edit</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item trash">Delete item</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="list-item" data-id="20">
                                        <div>
                                            <label className="ui-check m-0">
                                                <input type="checkbox" name="id" value="20"/>
                                                <i></i>
                                            </label>
                                        </div>
                                        <div>
                                            <a href="#" data-toggle-class>
                                                <BookmarkBorderIcon/>
                                                </a>
                                        </div>
                                        <div>
                                            <a href="#">
                                                <span className="w-40 avatar gd-warning" data-toggle-className="loading">
                                                    <span className="avatar-status on b-white avatar-right"></span> G</span>
                                            </a>
                                        </div>
                                        <div className="flex">
                                            <a href="#" className="item-author text-color">Gloria Williams</a>
                                            <div className="item-mail text-muted h-1x d-none d-sm-block">gloria-williams@netflix.com</div>
                                            <div className="item-tag tag hide">Company,Clients,Team,Personal,Work,Partners,Suppliers,Friends</div>
                                        </div>
                                        <div>
                                            <div className="item-action dropdown">
                                                <a href="#" data-toggle="dropdown" className="text-muted">
                                                    <MoreVertIcon/>
                                                </a>
                                                <div className="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                                    <a className="dropdown-item" href="#">See detail</a>
                                                    <a className="dropdown-item download">Download</a>
                                                    <a className="dropdown-item edit">Edit</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item trash">Delete item</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="list-item" data-id="13">
                                        <div>
                                            <label className="ui-check m-0">
                                                <input type="checkbox" name="id" value="13"/>
                                                <i></i>
                                            </label>
                                        </div>
                                        <div>
                                            <a href="#" data-toggle-class>
                                                <BookmarkBorderIcon/>
                                                </a>
                                        </div>
                                        <div>
                                            <a href="#">
                                                <span className="w-40 avatar gd-primary" data-toggle-className="loading">
                                                    <span className="avatar-status off b-white avatar-right"></span> L</span>
                                            </a>
                                        </div>
                                        <div className="flex">
                                            <a href="#" className="item-author text-color">Lisa Chapman</a>
                                            <div className="item-mail text-muted h-1x d-none d-sm-block">lisa-chapman@joejoecompany.com</div>
                                            <div className="item-tag tag hide">Personal,Company,Partners,Suppliers,Clients,Friends,Team,Work</div>
                                        </div>
                                        <div>
                                            <div className="item-action dropdown">
                                                <a href="#" data-toggle="dropdown" className="text-muted">
                                                    <MoreVertIcon/>
                                                </a>
                                                <div className="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                                    <a className="dropdown-item" href="#">See detail</a>
                                                    <a className="dropdown-item download">Download</a>
                                                    <a className="dropdown-item edit">Edit</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item trash">Delete item</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="list-item" data-id="6">
                                        <div>
                                            <label className="ui-check m-0">
                                                <input type="checkbox" name="id" value="6"/>
                                                <i></i>
                                            </label>
                                        </div>
                                        <div>
                                            <a href="#" data-toggle-class>
                                                <BookmarkBorderIcon/>
                                                </a>
                                        </div>
                                        <div>
                                            <a href="#">
                                                <span className="w-40 avatar gd-danger" data-toggle-className="loading">
                                                    <span className="avatar-status on b-white avatar-right"></span>
                                                    <img src="../assets/img/a6.jpg" alt="."/>
                                                </span>
                                            </a>
                                        </div>
                                        <div className="flex">
                                            <a href="#" className="item-author text-color">Wayne Gray</a>
                                            <div className="item-mail text-muted h-1x d-none d-sm-block">wayne-gray@google.com</div>
                                            <div className="item-tag tag hide">Partners,Company,Work,Personal,Friends,Clients,Suppliers,Team</div>
                                        </div>
                                        <div>
                                            <div className="item-action dropdown">
                                                <a href="#" data-toggle="dropdown" className="text-muted">
                                                    <MoreVertIcon/>
                                                </a>
                                                <div className="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                                    <a className="dropdown-item" href="#">See detail</a>
                                                    <a className="dropdown-item download">Download</a>
                                                    <a className="dropdown-item edit">Edit</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item trash">Delete item</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="list-item" data-id="9">
                                        <div>
                                            <label className="ui-check m-0">
                                                <input type="checkbox" name="id" value="9"/>
                                                <i></i>
                                            </label>
                                        </div>
                                        <div>
                                            <a href="#" data-toggle-class>
                                                <BookmarkBorderIcon/>
                                                </a>
                                        </div>
                                        <div>
                                            <a href="#">
                                                <span className="w-40 avatar gd-info" data-toggle-className="loading">
                                                    <span className="avatar-status on b-white avatar-right"></span>
                                                    <img src="../assets/img/a9.jpg" alt="."/>
                                                </span>
                                            </a>
                                        </div>
                                        <div className="flex">
                                            <a href="#" className="item-author text-color">Steven Cruz</a>
                                            <div className="item-mail text-muted h-1x d-none d-sm-block">steven-cruz@hhhinc.com</div>
                                            <div className="item-tag tag hide">Suppliers,Clients,Partners,Work,Team,Personal,Company,Friends</div>
                                        </div>
                                        <div>
                                            <div className="item-action dropdown">
                                                <a href="#" data-toggle="dropdown" className="text-muted">
                                                    <MoreVertIcon/>
                                                </a>
                                                <div className="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                                    <a className="dropdown-item" href="#">See detail</a>
                                                    <a className="dropdown-item download">Download</a>
                                                    <a className="dropdown-item edit">Edit</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item trash">Delete item</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="list-item" data-id="17">
                                        <div>
                                            <label className="ui-check m-0">
                                                <input type="checkbox" name="id" value="17"/>
                                                <i></i>
                                            </label>
                                        </div>
                                        <div>
                                            <a href="#" data-toggle-class>
                                                <BookmarkBorderIcon/>
                                                </a>
                                        </div>
                                        <div>
                                            <a href="#">
                                                <span className="w-40 avatar gd-warning" data-toggle-className="loading">
                                                    <span className="avatar-status on b-white avatar-right"></span> H</span>
                                            </a>
                                        </div>
                                        <div className="flex">
                                            <a href="#" className="item-author text-color">Helen Valdez</a>
                                            <div className="item-mail text-muted h-1x d-none d-sm-block">helen-valdez@ai.com</div>
                                            <div className="item-tag tag hide">Friends,Partners,Clients,Company,Personal,Team,Suppliers,Work</div>
                                        </div>
                                        <div>
                                            <div className="item-action dropdown">
                                                <a href="#" data-toggle="dropdown" className="text-muted">
                                                    <MoreVertIcon/>
                                                </a>
                                                <div className="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                                    <a className="dropdown-item" href="#">See detail</a>
                                                    <a className="dropdown-item download">Download</a>
                                                    <a className="dropdown-item edit">Edit</a>
                                                    <div className="dropdown-divider"></div>
                                                    <a className="dropdown-item trash">Delete item</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="no-result hide">
                                    <div className="p-4 text-center">No Results</div>
                                </div>
                            </Card>
                            <div className="px-3 py-3 mt-auto">
                                <div className="d-flex align-items-center">
                                    <div className="flex d-flex flex-row">
                                        <button className="btn btn-sm no-bg no-shadow px-0 pager-prev">
                                            <ChevronLeftIcon/> 
                                        </button>
                                        <div className="pagination pagination-sm mx-1"></div>
                                        <button className="btn btn-sm no-bg no-shadow px-0 pager-next">
                                            <ChevronRightIcon/>
                                        </button>
                                    </div>
                                    <div>
                                        <span className="text-muted">Total:</span>
                                        <span id="count"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
)
}

export default User;