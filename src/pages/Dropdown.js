import React from 'react';
import '../assets/site.min.css';


function Dropdown() {
    return (
        <div>
            <div id="main" class="layout-column flex">
                <div id="header" class="page-header">
                    <div class="navbar navbar-expand-lg">
                        <a href="index.html" class="navbar-brand d-lg-none"><svg width="32" height="32"
                                viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" fill="currentColor">
                                <g class="loading-spin" style={{transformOrigin: "256px 256px"}}>
                                    <path
                                        d="M200.043 106.067c-40.631 15.171-73.434 46.382-90.717 85.933H256l-55.957-85.933zM412.797 288A160.723 160.723 0 0 0 416 256c0-36.624-12.314-70.367-33.016-97.334L311 288h101.797zM359.973 134.395C332.007 110.461 295.694 96 256 96c-7.966 0-15.794.591-23.448 1.715L310.852 224l49.121-89.605zM99.204 224A160.65 160.65 0 0 0 96 256c0 36.639 12.324 70.394 33.041 97.366L201 224H99.204zM311.959 405.932c40.631-15.171 73.433-46.382 90.715-85.932H256l55.959 85.932zM152.046 377.621C180.009 401.545 216.314 416 256 416c7.969 0 15.799-.592 23.456-1.716L201.164 288l-49.118 89.621z">
                                    </path>
                                </g>
                            </svg><span
                                class="hidden-folded d-inline l-s-n-1x d-lg-none">Basik</span> </a>
                        <div class="collapse navbar-collapse order-2 order-lg-1" id="navbarToggler">
                            <form class="input-group m-2 my-lg-0">
                                <div class="input-group-prepend"><button type="button"
                                        class="btn no-shadow no-bg px-0 text-inherit"><i data-feather="search"></i></button>
                                </div><input type="text" class="form-control no-border no-shadow no-bg typeahead"
                                    placeholder="Search components..." data-plugin="typeahead"
                                    data-api="../assets/api/menu.json"/>
                            </form>
                        </div>
                        <ul class="nav navbar-menu order-1 order-lg-2">
                            <li class="nav-item d-none d-sm-block"><a class="nav-link px-2" data-toggle="fullscreen"
                                    data-plugin="fullscreen"><i data-feather="maximize"></i></a></li>
                            <li class="nav-item dropdown"><a class="nav-link px-2" data-toggle="dropdown"><i
                                        data-feather="settings"></i> </a>
                                <div class="dropdown-menu dropdown-menu-center mt-3 w-md animate fadeIn">
                                    <div class="setting px-3">
                                        <div class="mb-2 text-muted"><strong>Setting:</strong></div>
                                        <div class="mb-3" id="settingLayout"><label
                                                class="ui-check ui-check-rounded my-1 d-block"><input type="checkbox"
                                                    name="stickyHeader"/> <i></i> <small>Sticky header</small></label><label
                                                class="ui-check ui-check-rounded my-1 d-block"><input type="checkbox"
                                                    name="stickyAside"/> <i></i> <small>Sticky aside</small></label><label
                                                class="ui-check ui-check-rounded my-1 d-block"><input type="checkbox"
                                                    name="foldedAside"/> <i></i> <small>Folded Aside</small></label><label
                                                class="ui-check ui-check-rounded my-1 d-block"><input type="checkbox"
                                                    name="hideAside"/> <i></i> <small>Hide Aside</small></label></div>
                                        <div class="mb-2 text-muted"><strong>Color:</strong></div>
                                        <div class="mb-2"><label class="radio radio-inline ui-check ui-check-md"><input
                                                    type="radio" name="bg" value=""/> <i></i></label><label
                                                class="radio radio-inline ui-check ui-check-color ui-check-md"><input
                                                    type="radio" name="bg" value="bg-dark"/> <i class="bg-dark"></i></label>
                                        </div>
                                        <div class="mb-2 text-muted"><strong>Layouts:</strong></div>
                                        <div class="mb-3"><a href="dashboard.html"
                                                class="btn btn-xs btn-white no-ajax mb-1">Default</a> <a
                                                href="layout.a5523.html?bg" class="btn btn-xs btn-primary no-ajax mb-1">A</a> <a
                                                href="layout.b5523.html?bg" class="btn btn-xs btn-info no-ajax mb-1">B</a> <a
                                                href="layout.c5523.html?bg" class="btn btn-xs btn-success no-ajax mb-1">C</a> <a
                                                href="layout.d5523.html?bg" class="btn btn-xs btn-warning no-ajax mb-1">D</a>
                                        </div>
                                        <div class="mb-2 text-muted"><strong>Apps:</strong></div>
                                        <div><a href="dashboard.html" class="btn btn-sm btn-white no-ajax mb-1">Dashboard</a> <a
                                                href="music5523.html?bg" class="btn btn-sm btn-white no-ajax mb-1">Music</a> <a
                                                href="video5523.html?bg" class="btn btn-sm btn-white no-ajax mb-1">Video</a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown"><a class="nav-link px-2 mr-lg-2" data-toggle="dropdown"><i
                                        data-feather="bell"></i> <span class="badge badge-pill badge-up bg-primary">8</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mt-3 w-md animate fadeIn p-0">
                                    <div class="scrollable hover" style={{maxHeight: "250px"}}>
                                        <div class="list list-row">
                                            <div class="list-item" data-id="7">
                                                <div><a href="#"><span class="w-32 avatar gd-primary"><img
                                                                src="../assets/img/a7.jpg" alt="."/></span></a></div>
                                                <div class="flex">
                                                    <div class="item-feed h-2x">From design to dashboard, <a href="#">@Dash</a>
                                                        builds custom hardware according to on-site requirements</div>
                                                </div>
                                            </div>
                                            <div class="list-item" data-id="13">
                                                <div><a href="#"><span class="w-32 avatar gd-primary">L</span></a></div>
                                                <div class="flex">
                                                    <div class="item-feed h-2x">Thanks! Awesome to see good support on Twitter
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list-item" data-id="14">
                                                <div><a href="#"><span class="w-32 avatar gd-warning">B</span></a></div>
                                                <div class="flex">
                                                    <div class="item-feed h-2x">Do you know which are the popular ones? Leave a
                                                        comment and get to know more from professional developers</div>
                                                </div>
                                            </div>
                                            <div class="list-item" data-id="11">
                                                <div><a href="#"><span class="w-32 avatar gd-info">K</span></a></div>
                                                <div class="flex">
                                                    <div class="item-feed h-2x">Prepare the documentation for the <a
                                                            href="#">Fitness app</a></div>
                                                </div>
                                            </div>
                                            <div class="list-item" data-id="6">
                                                <div><a href="#"><span class="w-32 avatar gd-danger"><img
                                                                src="../assets/img/a6.jpg" alt="."/></span></a></div>
                                                <div class="flex">
                                                    <div class="item-feed h-2x">Just saw this on the <a href="#">@eBay</a>
                                                        dashboard, dude is an absolute unit.</div>
                                                </div>
                                            </div>
                                            <div class="list-item" data-id="10">
                                                <div><a href="#"><span class="w-32 avatar gd-danger"><img
                                                                src="../assets/img/a10.jpg" alt="."/></span></a></div>
                                                <div class="flex">
                                                    <div class="item-feed h-2x">Developers of <a href="#">@iAI</a>, the AI
                                                        assistant based on Free Software</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex px-3 py-2 b-t">
                                        <div class="flex"><span>6 Notifications</span></div><a href="page.setting.html">See all
                                            <i class="fa fa-angle-right text-muted"></i></a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown"><a href="#" data-toggle="dropdown"
                                    class="nav-link d-flex align-items-center px-2 text-color"><span class="avatar w-24"
                                        style={{margin: "-2px"}}><img src="../assets/img/a2.jpg" alt="..."/></span></a>
                                <div class="dropdown-menu dropdown-menu-right w mt-3 animate fadeIn"><a class="dropdown-item"
                                        href="page.profile.html"><span>Jacqueline Reid</span> </a><a class="dropdown-item"
                                        href="page.price.html"><span class="badge bg-success text-uppercase">Upgrade</span>
                                        <span>to Pro</span></a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item"
                                        href="page.profile.html"><span>Profile</span> </a><a class="dropdown-item d-flex"
                                        href="page.invoice.html"><span class="flex">Invoice</span> <span><b
                                                class="badge badge-pill gd-warning">5</b></span> </a><a class="dropdown-item"
                                        href="page.faq.html">Need help?</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item"
                                        href="page.setting.html"><span>Account Settings</span> </a><a class="dropdown-item"
                                        href="signin.1.html">Sign out</a>
                                </div>
                            </li>
                            <li class="nav-item d-lg-none"><a href="#" class="nav-link px-2" data-toggle="collapse"
                                    data-toggle-class data-target="#navbarToggler"><i data-feather="search"></i></a></li>
                            <li class="nav-item d-lg-none"><a class="nav-link px-1" data-toggle="modal" data-target="#aside"><i
                                        data-feather="menu"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div id="content" class="flex">
                    <div>
                        <div class="page-hero page-container" id="page-hero">
                            <div class="padding d-flex">
                                <div class="page-title">
                                    <h2 class="text-md text-highlight">Dropdown</h2><small class="text-muted">Toggle contextual
                                        overlays for displaying lists of links and more</small>
                                </div>
                                <div class="flex"></div>

                            </div>
                        </div>
                        <div class="page-content page-container" id="page-content">
                            <div class="padding">
                                <h6>Dropdown menu</h6>
                                <div class="d-flex flex-wrap mb-4">
                                    <div class="dropdown mb-3">
                                        <ul class="dropdown-menu pos-stc d-inline" role="menu">
                                            <h6 class="dropdown-header">Dropdown header</h6>
                                            <li class="dropdown-item"><a href="#">Action</a></li>
                                            <li class="dropdown-item"><a href="#">Another action</a></li>
                                            <li class="dropdown-item"><a href="#">Something else here</a></li>
                                            <li class="dropdown-divider"></li>
                                            <li class="dropdown-item"><a href="#">Separated link</a></li>
                                            <li class="dropdown-item dropdown-submenu"><a href="#">Submenu</a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li class="dropdown-item"><a href="#">Action</a></li>
                                                    <li class="dropdown-item"><a href="#">Another action</a></li>
                                                    <li class="dropdown-item"><a href="#">Something else here</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="dropdown mb-3 mx-3">
                                        <ul class="dropdown-menu pos-stc d-inline bg-dark" role="menu">
                                            <h6 class="dropdown-header">Dropdown header</h6>
                                            <li class="dropdown-item"><a href="#">Action</a></li>
                                            <li class="dropdown-item"><a href="#">Another action</a></li>
                                            <li class="dropdown-item"><a href="#">Something else here</a></li>
                                            <li class="dropdown-divider"></li>
                                            <li class="dropdown-item"><a href="#">Separated link</a></li>
                                            <li class="dropdown-item dropdown-submenu"><a href="#">Submenu</a>
                                                <ul class="dropdown-menu pull-left" role="menu">
                                                    <li class="dropdown-item"><a href="#">Action</a></li>
                                                    <li class="dropdown-item"><a href="#">Another action</a></li>
                                                    <li class="dropdown-item"><a href="#">Something else here</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="dropdown mb-3">
                                        <ul class="dropdown-menu pos-stc d-inline bg-secondary" role="menu">
                                            <h6 class="dropdown-header">Dropdown header</h6>
                                            <li class="dropdown-item"><a href="#">Action</a></li>
                                            <li class="dropdown-item"><a href="#">Another action</a></li>
                                            <li class="dropdown-item"><a href="#">Something else here</a></li>
                                            <li class="dropdown-divider"></li>
                                            <li class="dropdown-item"><a href="#">Separated link</a></li>
                                            <li class="dropdown-item dropdown-submenu"><a href="#">Submenu</a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li class="dropdown-item"><a href="#">Action</a></li>
                                                    <li class="dropdown-item"><a href="#">Another action</a></li>
                                                    <li class="dropdown-item"><a href="#">Something else here</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <h6>Dropdown icon</h6>
                                <div class="d-flex flex-wrap mb-4">
                                    <div class="dropdown mb-3">
                                        <div class="dropdown-menu pos-stc d-inline" role="menu"><a class="dropdown-item"><i
                                                    class="badge badge-circle xs text-primary"></i> Workshop </a><a
                                                class="dropdown-item"><i class="badge badge-circle xs text-info"></i> Enterprise
                                            </a><a class="dropdown-item"><i class="badge badge-circle xs text-success"></i>
                                                Personal </a><a class="dropdown-item"><i
                                                    class="badge badge-circle xs text-warning"></i> Private</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item"><i
                                                    data-feather="plus"></i> Create new label</a>
                                        </div>
                                    </div>
                                    <div class="dropdown mb-3 mx-3">
                                        <div class="dropdown-menu pos-stc d-inline" role="menu"><a class="dropdown-item"><i
                                                    data-feather="plus"></i> Action </a><a class="dropdown-item"><i
                                                    data-feather="minus"></i> Another action </a><a class="dropdown-item"><i
                                                    data-feather="arrow-down"></i> Something else here</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item"><i
                                                    data-feather="x"></i> Separated link </a><a class="dropdown-item"><i
                                                    data-feather="more-vertical"></i> More</a>
                                        </div>
                                    </div>
                                    <div class="dropdown mb-3">
                                        <div class="dropdown-menu pos-stc d-inline bg-primary" role="menu"><a
                                                class="dropdown-item"><i data-feather="plus"></i> Action </a><a
                                                class="dropdown-item"><i data-feather="minus"></i> Another action </a><a
                                                class="dropdown-item"><i data-feather="arrow-down"></i> Something else here</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item"><i
                                                    data-feather="x"></i> Separated link </a><a class="dropdown-item"><i
                                                    data-feather="more-vertical"></i> More</a>
                                        </div>
                                    </div>
                                </div>
                                <h6>Single button dropdowns</h6>
                                <div class="d-inline-flex mb-4 toolbar">
                                    <div class="dropdown mb-2"><button class="btn btn-white dropdown-toggle"
                                            data-toggle="dropdown">Dropdown</button>
                                        <div class="dropdown-menu bg-dark" role="menu"><a class="dropdown-item">Action </a><a
                                                class="dropdown-item">Another action </a><a class="dropdown-item">Something else
                                                here</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item">Separated link</a>
                                        </div>
                                    </div>
                                    <div class="dropdown dropup"><button class="btn btn-white dropdown-toggle"
                                            data-toggle="dropdown">Dropup</button>
                                        <div class="dropdown-menu bg-dark" role="menu"><a class="dropdown-item">Action </a><a
                                                class="dropdown-item">Another action </a><a class="dropdown-item">Something else
                                                here</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item">Separated link</a>
                                        </div>
                                    </div>
                                </div>
                                <h6>Split button dropdowns &amp; variation</h6>
                                <div class="d-inline-flex mb-4 toolbar">
                                    <div class="mb-2">
                                        <div class="btn-group"><button class="btn btn-white">Dropdown</button> <button
                                                class="btn btn-white dropdown-toggle" data-toggle="dropdown"></button>
                                            <div class="dropdown-menu bg-dark" role="menu"><a class="dropdown-item">Action
                                                </a><a class="dropdown-item">Another action </a><a
                                                    class="dropdown-item">Something else here</a>
                                                <div class="dropdown-divider"></div><a class="dropdown-item">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="btn-group"><button class="btn btn-white">Dropdown right</button> <button
                                                class="btn btn-white dropdown-toggle" data-toggle="dropdown"></button>
                                            <div class="dropdown-menu dropdown-menu-right" role="menu"><a
                                                    class="dropdown-item">Action </a><a class="dropdown-item">Another action
                                                </a><a class="dropdown-item">Something else here</a>
                                                <div class="dropdown-divider"></div><a class="dropdown-item">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="btn-group dropup"><button class="btn btn-white">Dropup</button> <button
                                                class="btn btn-white dropdown-toggle" data-toggle="dropdown"></button>
                                            <div class="dropdown-menu bg-dark" role="menu"><a class="dropdown-item">Action
                                                </a><a class="dropdown-item">Another action </a><a
                                                    class="dropdown-item">Something else here</a>
                                                <div class="dropdown-divider"></div><a class="dropdown-item">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="btn-group dropup"><button class="btn btn-white">Dropup right</button>
                                            <button class="btn btn-white dropdown-toggle" data-toggle="dropdown"></button>
                                            <div class="dropdown-menu dropdown-menu-right" role="menu"><a
                                                    class="dropdown-item">Action </a><a class="dropdown-item">Another action
                                                </a><a class="dropdown-item">Something else here</a>
                                                <div class="dropdown-divider"></div><a class="dropdown-item">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h6>Dropleft &amp; Dropright</h6>
                                <div class="d-inline-flex mb-4 toolbar">
                                    <div class="mb-2">
                                        <div class="btn-group dropright"><button class="btn btn-white dropdown-toggle"
                                                data-toggle="dropdown">Dropright</button>
                                            <div class="dropdown-menu bg-dark" role="menu"><a class="dropdown-item">Action
                                                </a><a class="dropdown-item">Another action </a><a
                                                    class="dropdown-item">Something else here</a>
                                                <div class="dropdown-divider"></div><a class="dropdown-item">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="btn-group dropright"><button class="btn btn-white">Split dropright</button>
                                            <button class="btn btn-white dropdown-toggle" data-toggle="dropdown"></button>
                                            <div class="dropdown-menu bg-dark" role="menu"><a class="dropdown-item">Action
                                                </a><a class="dropdown-item">Another action </a><a
                                                    class="dropdown-item">Something else here</a>
                                                <div class="dropdown-divider"></div><a class="dropdown-item">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="btn-group dropleft"><button class="btn btn-white dropdown-toggle"
                                                data-toggle="dropdown">Dropleft</button>
                                            <div class="dropdown-menu bg-dark" role="menu"><a class="dropdown-item">Action
                                                </a><a class="dropdown-item">Another action </a><a
                                                    class="dropdown-item">Something else here</a>
                                                <div class="dropdown-divider"></div><a class="dropdown-item">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2">
                                        <div class="btn-group">
                                            <div class="btn-group dropleft"><button class="btn btn-white dropdown-toggle"
                                                    data-toggle="dropdown"></button>
                                                <div class="dropdown-menu bg-dark" role="menu"><a class="dropdown-item">Action
                                                    </a><a class="dropdown-item">Another action </a><a
                                                        class="dropdown-item">Something else here</a>
                                                    <div class="dropdown-divider"></div><a class="dropdown-item">Separated
                                                        link</a>
                                                </div>
                                            </div><button class="btn btn-white">Split dropleft</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Dropdown;
