import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import Dashboard from "./pages/Dashboard";
import Carousel from "./pages/Carousel";
import Dropdown from "./pages/Dropdown";
import Form from "./pages/Form";
import Profile from "./pages/Profile";
import Sortable from "./pages/Sortable";
import User from "./pages/User";
import Sidebar from './bars/Sidebar';


function App() {
  return (
    <BrowserRouter>
       <Switch>
        <Route path={"/"} component={Sidebar} />
        <Route exact path={"/user"} component={User} />
        <Route exact path={"/form"} component={Form} />
        <Route exact path={"/profile"} component={Profile} />
        <Route exact path={"/carousel"} component={Carousel} />
        <Route exact path={"/dropdown"} component={Dropdown} />
        <Route exact path={"/sortable"} component={Sortable} />
        <Route exact path={"/dashboard"} component={Dashboard} />
      </Switch> 
    </BrowserRouter>
  );
}

export default App;