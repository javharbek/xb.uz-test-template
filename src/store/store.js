import {createStore} from 'redux';
import { connectRouter } from 'connected-react-router';



const createBrowserHistory = require('history').createBrowserHistory;

export const history = createBrowserHistory();

export default createStore(connectRouter(history)(rootReducer),composedEnhancers);