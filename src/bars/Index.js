import React from 'react'

import Sidebar from './Sidebar'
import SearchBar from './SearchBar'
function Index() {
    return (
        <div style={{display:"flex"}}>
            <div className="page-sidenav no-shrink bg-light nav-dropdown fade">
                <Sidebar/>
            </div>
            <SearchBar/>
        </div>
  
    )
}

export default Index
