import React from 'react';
import '../assets/site.min.css';
import PeopleIcon from '@material-ui/icons/People';
import DashboardIcon from '@material-ui/icons/Dashboard';
import SortByAlphaIcon from '@material-ui/icons/SortByAlpha';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import ViewCarouselIcon from '@material-ui/icons/ViewCarousel';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ArrowDropDownCircleIcon from '@material-ui/icons/ArrowDropDownCircle';
import FormatIndentIncreaseIcon from '@material-ui/icons/FormatIndentIncrease';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import { Link } from "react-router-dom";
import Card from '@material-ui/core/Card';



import { Route } from "react-router-dom";

import Dashboard from "../pages/Dashboard";
import Carousel from "../pages/Carousel";
import Dropdown from "../pages/Dropdown";
import Form from "../pages/Form";
import Profile from "../pages/Profile";
import Sortable from "../pages/Sortable";
import User from "../pages/User";
import SearchBar from "./SearchBar";

import { useHistory } from 'react-router-dom';

const routes = [
  {
    path: "/dashboard",
    exact: true,
    main: () => <Dashboard />,
  },
  {
    path: "/carousel",
    exact: true,
    main: () => <Carousel />,
  },
  {
    path: "/form",
    exact: true,
    main: () => <Form />,
  },
  {
    path: "/profile",
    exact: true,
    main: () => <Profile />,
  },
  {
    path: "/sortable",
    exact: true,
    main: () => <Sortable />,
  },
  {
    path: "/user",
    exact: true,
    main: () => <User />,
  },
  {
    path: "/dropdown",
    exact: true,
    main: () => <Dropdown />,
  },
];

function Sidebar() {
  const history = useHistory();
  return (
    <div style={{ display: "flex", height: "40%" }}>
      <div className="page-sidenav no-shrink bg-light nav-dropdown fade">
        <div className="page-sidenav no-shrink bg-light nav-dropdown fade" >
          <div className="sidenav vh-100 modal-dialog bg-light">
            <div className="navbar navbar-expand-lg">
              <Link to={'/'} className="navbar-brand">
                <svg width="32" height="32" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/2000/xlink" fill="currentColor">
                  <g className="loading-spin" style={{ transformOrigin: "256px 256px" }}>
                    <path d="M200.043 106.067c-40.631 15.171-73.434 46.382-90.717 85.933H256l-55.957-85.933zM412.797 288A160.723 160.723 0 0 0 416 256c0-36.624-12.314-70.367-33.016-97.334L311 288h101.797zM359.973 134.395C332.007 110.461 295.694 96 256 96c-7.966 0-15.794.591-23.448 1.715L310.852 224l49.121-89.605zM99.204 224A160.65 160.65 0 0 0 96 256c0 36.639 12.324 70.394 33.041 97.366L201 224H99.204zM311.959 405.932c40.631-15.171 73.433-46.382 90.715-85.932H256l55.959 85.932zM152.046 377.621C180.009 401.545 216.314 416 256 416c7.969 0 15.799-.592 23.456-1.716L201.164 288l-49.118 89.621z"></path>
                  </g>
                </svg>
                <span className="hidden-folded d-inline l-s-n-1x">Basik</span>
              </Link>
            </div>
            <div className="flex scrollable hover ">
              <div className="nav-active-text-primary" data-nav>
                <List className="nav bg">
                  <ListItem className="nav-header hidden-folded"><span className="text-muted">Main</span></ListItem>
                  <ListItem button onClick={() => history.push('/dashboard')}><span className="nav-icon text-primary"><DashboardIcon /></span> <span className="nav-text"> Dashboard</span></ListItem>
                  <ListItem button onClick={() => history.push('/user')}><span className="nav-icon text-success"><PeopleIcon /> </span> <span className="nav-text">Users</span></ListItem>
                  <ListItem button onClick={() => history.push('/profile')}><span className="nav-icon text-warning"><AccountCircleIcon /> </span> <span className="nav-text">Profile</span></ListItem>
                  <ListItem className="nav-header hidden-folded"><span className="text-muted">Ui Elements</span></ListItem>
                  <ListItem button onClick={() => history.push('/carousel')}><span className="nav-icon text-warning"><ViewCarouselIcon /> </span> <span className="nav-text">Carousel</span></ListItem>
                  <ListItem button onClick={() => history.push('/dropdown')}><span className="nav-icon text-success"><ArrowDropDownCircleIcon /> </span> <span className="nav-text">Dropdown</span></ListItem>
                  <ListItem button onClick={() => history.push('/sidenav')}><span className="nav-icon text-warning"><MenuOpenIcon /> </span> <span className="nav-text">Sidenav</span></ListItem>
                  <ListItem button onClick={() => history.push('/sortable')}><span className="nav-icon text-warning"><SortByAlphaIcon /> </span> <span className="nav-text">Sortable</span></ListItem>
                  <ListItem button onClick={() => history.push('/form')}><span className="nav-icon text-warning"><FormatIndentIncreaseIcon /> </span> <span className="nav-text">Form Element</span></ListItem>
                </List>
              </div>
            </div>
            <div className="no-shrink">
              <div className="p-3 d-flex align-items-center">
                <div className="text-sm hidden-folded text-muted">Trial: 35%</div>
                <div className="progress mx-2 flex" style={{ height: "4px" }}>
                  <div className="progress-bar gd-success" style={{ width: "35%" }}></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style={{width: "100%" }}>
        <SearchBar/>
        <Card style={{}}>
          {routes.map((route, index) => (
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              component={route.main}
            />
          ))}
        </Card>
      </div>
    </div>
  )
}

export default Sidebar